package dev.tacon.presenter;

import java.util.List;
import java.util.Map;

import dev.tacon.presenter.display.Display;
import dev.tacon.presenter.display.parameter.InvalidDisplayParameterException;

public abstract class AbstractPresenter<D extends Display> {

	private D display;

	/**
	 * Returns the current managed display instance.
	 */
	public D getDisplay() {
		return this.display;
	}

	/**
	 * Makes the specified display managed by this presenter.
	 * If called twice passing the same display instance it does
	 * nothing. If a second display instance is passed an
	 * {@code IllegalArgumentException} is thrown.
	 *
	 * @param display
	 * @throws IllegalStateException if the presenter is already
	 *             managing another display instance.
	 */
	public final void setDisplay(final D display) {
		if (this.display != null) {
			if (this.display != display) {
				throw new IllegalStateException("This presenter is already managing a display: " + this.display);
			}
			// display has been already initialized, skipping first configuration
			return;
		}
		this.display = display;
		this.configureDisplayFirstTime();
	}

	void configureDisplayFirstTime() {
		this.initDisplay();
	}

	/**
	 * Sets the navigation parameters for this presenter.
	 * <p>
	 * May throw an {@linkplain InvalidDisplayParameterException} in
	 * case of invalid parameters.
	 *
	 * @param parameters navigation parameters.
	 * @throws InvalidDisplayParameterException if any parameter
	 *             has an invalid value.
	 */
	public void setParameters(final Map<String, List<String>> parameters) {
		// no-op by default
	}

	/**
	 * Performs actions before leaving the underlying view.
	 * <p>
	 * The provided {@code Runnable} must be called in
	 * order to proceed further.
	 *
	 * @param leave action to proceed further.
	 */
	public void onLeave(final Runnable proceedCallback) {
		proceedCallback.run();
	}

	/**
	 * Initializes the managed display.
	 * During initialization {@link #getDisplay()} will
	 * always return a valid display instance.
	 */
	protected abstract void initDisplay();
}
