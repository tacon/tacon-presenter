package dev.tacon.presenter;

public class DataPresenterException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DataPresenterException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DataPresenterException(final String message) {
		super(message);
	}

	public DataPresenterException(final Throwable cause) {
		super(cause);
	}
}
