package dev.tacon.presenter;

/**
 * Type of operation on the data.
 */
public enum DataOperation {
	INSERT,
	EDIT,
	VIEW,
	DELETE;
}