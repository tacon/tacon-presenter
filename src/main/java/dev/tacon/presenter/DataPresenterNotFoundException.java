package dev.tacon.presenter;

public class DataPresenterNotFoundException extends DataPresenterException {

	private static final long serialVersionUID = 1L;

	private final DataOperation operation;
	private final String id;

	public DataPresenterNotFoundException(final DataOperation operation, final String id) {
		super("Data not found with id " + id + " for operation " + operation);
		this.operation = operation;
		this.id = id;
	}

	public DataOperation getOperation() {
		return this.operation;
	}

	public String getId() {
		return this.id;
	}
}
