package dev.tacon.presenter.display;

import dev.tacon.annotations.NonNullByDefault;

@NonNullByDefault
public interface Display {

	public enum Type {
		INFO,
		WARN,
		ERROR,
		SUCCESS;
	}

	void notifyMessage(String message, Type type);

	void showMessage(String message, Type type, Runnable callback);

	default void showMessage(final String message, final Type type) {
		this.showMessage(message, type, () -> {});
	}

	void showError(Exception ex, Runnable callback);

	default void showError(final Exception ex) {
		this.showError(ex, () -> {});
	}
}
