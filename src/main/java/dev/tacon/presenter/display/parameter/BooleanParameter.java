package dev.tacon.presenter.display.parameter;

import dev.tacon.annotations.NonNull;

class BooleanParameter extends DisplayParameter<Boolean> {

	BooleanParameter(final String name) {
		super(name);
	}

	@Override
	public Boolean deserialize(final @NonNull String string) {
		return Boolean.valueOf(string);
	}
}
