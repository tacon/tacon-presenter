package dev.tacon.presenter.display.parameter;

import java.util.UUID;

import dev.tacon.annotations.NonNull;

class UUIDParameter extends DisplayParameter<UUID> {

	UUIDParameter(final String name) {
		super(name);
	}

	@Override
	public UUID deserialize(final @NonNull String string) {
		return UUID.fromString(string);
	}
}
