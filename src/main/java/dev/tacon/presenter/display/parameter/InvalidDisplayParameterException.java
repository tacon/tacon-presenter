package dev.tacon.presenter.display.parameter;

import java.util.Optional;

/**
 * Exception raised when an invalid parameter reaches the presenter.
 */
public class InvalidDisplayParameterException extends RuntimeException {

	private static final long serialVersionUID = -1813072513661891252L;

	private final String parameterName;
	private final String invalidValue;

	public InvalidDisplayParameterException(final String parameterName) {
		this(parameterName, (String) null);
	}

	public InvalidDisplayParameterException(final String parameterName, final String invalidValue) {
		super("Invalid parameter " + parameterName);
		this.parameterName = parameterName;
		this.invalidValue = invalidValue;
	}

	public InvalidDisplayParameterException(final String parameterName, final Exception ex) {
		this(parameterName, null, ex);
	}

	public InvalidDisplayParameterException(final String parameterName, final String invalidValue, final Exception ex) {
		super("Invalid parameter " + parameterName, ex);
		this.parameterName = parameterName;
		this.invalidValue = invalidValue;
	}

	/**
	 * Returns the invalid parameter name.
	 */
	public String getParameterName() {
		return this.parameterName;
	}

	/**
	 * Returns the invalid parameter value,
	 * may not be present.
	 */
	public Optional<String> getInvalidValue() {
		return Optional.ofNullable(this.invalidValue);
	}
}
