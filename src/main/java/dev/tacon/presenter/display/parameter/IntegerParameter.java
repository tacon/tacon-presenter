package dev.tacon.presenter.display.parameter;

import dev.tacon.annotations.NonNull;

class IntegerParameter extends DisplayParameter<Integer> {

	IntegerParameter(final String name) {
		super(name);
	}

	@Override
	public Integer deserialize(final @NonNull String string) {
		return Integer.valueOf(string);
	}
}
