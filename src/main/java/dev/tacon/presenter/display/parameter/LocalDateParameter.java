package dev.tacon.presenter.display.parameter;

import java.time.LocalDate;

import dev.tacon.annotations.NonNull;

class LocalDateParameter extends DisplayParameter<LocalDate> {

	LocalDateParameter(final String name) {
		super(name);
	}

	@Override
	public LocalDate deserialize(final @NonNull String string) {
		return LocalDate.parse(string);
	}
}
