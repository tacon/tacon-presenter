package dev.tacon.presenter.display.parameter;

import dev.tacon.annotations.NonNull;

class EnumParameter<E extends Enum<E>> extends DisplayParameter<E> {

	private final Class<E> enumClass;

	EnumParameter(final String name, final Class<E> enumClass) {
		super(name);
		this.enumClass = enumClass;
	}

	@Override
	public E deserialize(final @NonNull String string) {
		return Enum.valueOf(this.enumClass, string);
	}
}
