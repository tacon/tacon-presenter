package dev.tacon.presenter.display.parameter;

import dev.tacon.annotations.NonNull;

class StringParameter extends DisplayParameter<String> {

	StringParameter(final String name) {
		super(name);
	}

	@Override
	public String deserialize(final @NonNull String string) {
		return string;
	}
}
