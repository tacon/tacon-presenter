package dev.tacon.presenter.display.parameter;

import java.math.BigDecimal;

import dev.tacon.annotations.NonNull;

class BigDecimalParameter extends DisplayParameter<BigDecimal> {

	BigDecimalParameter(final String name) {
		super(name);
	}

	@Override
	public BigDecimal deserialize(final @NonNull String string) {
		return new BigDecimal(string);
	}
}
