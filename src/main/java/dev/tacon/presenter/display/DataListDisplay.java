package dev.tacon.presenter.display;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import dev.tacon.annotations.NonNullByDefault;
import dev.tacon.annotations.Nullable;

@NonNullByDefault
public interface DataListDisplay<I> extends Display {

	void setOnInsert(Runnable callback);

	void setOnEdit(Runnable callback);

	void setOnView(Runnable callback);

	void setOnDelete(Runnable callback);

	void openData(@Nullable I sourceItem, Map<String, List<String>> parameters);

	Optional<I> getSelectedItem();

	Collection<I> getSelectedItems();

	void setItems(List<I> items);

	void askDelete(I item, Runnable confirmCallback);

	void onSuccessfulDelete(I item);
}
